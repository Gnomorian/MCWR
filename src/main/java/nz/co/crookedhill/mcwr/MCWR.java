package nz.co.crookedhill.mcwr;

import java.util.Iterator;
import java.util.Map;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.common.MinecraftForge;
import nz.co.crookedhill.mcwr.achievement.MCWRAchievement;
import nz.co.crookedhill.mcwr.creativetabs.MCWRCreativeMobs;
import nz.co.crookedhill.mcwr.creativetabs.MCWRCreativeTabItem;
import nz.co.crookedhill.mcwr.handler.MCWRConnectionHandler;
import nz.co.crookedhill.mcwr.helper.MCWRClassGetter;
import nz.co.crookedhill.mcwr.proxy.MCWRCommonProxy;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = MCWR.MODID, version = MCWR.VERSION)
public class MCWR
{
	public static final String MODID = "mcwr";
	public static final String VERSION = "0.0.3.2";
	private static final String ITEMDIR = "nz.co.crookedhill.mcwr.items";
	public static final String[] WELCOME = { "Now, go out there and have some fun!", "Don't let those stinky Floobs push you around!", "Give a diamond to a level 25 HotDog for a special reward!", "Urinating Bums can help with landscaping. Try one today!", "You're doing something right!", "Watch out for grumpy G's!", "Guinea Pigs make nice pets.", "Bring a lost Kid back to a Lolliman for a nice reward.", "Robot Ted thinks Robot Todd is a dirty chicken wing.", "Sneaky Sal changes his prices. Check back for bargains.", "Power your HotDog with redstone for a fire attack!", "You want money? Punch a Lawyer From Hell!", "Equip your HotDogs with Redstone for fire attacks!", "Guinea Pigs eat Wheat and Apples.", "A Floob Ship will spit out Floobs until it is destroyed.", "Drop a BubbleScum 100 blocks for the MERCILESS achievement!", "Throw a BubbleScum down a DigBug hole for a cookie fountain!", "Feed lots of cake to a Hunchback and he will stay loyal.", "The longer you ride a RocketPony, the more tame it will be.", "Visit Sneaky Sal for those hard to find items.", "Hitting a Caveman will turn him/her evil!", "SNEAK KEY + RIGHT CLICK on creeps for info or to name them.", "Give a level 20 Guinea Pig a diamond to build a Hotel!", "If you hear disco music - RUN!", "Raising your pets ATTACK skill will help them level faster.", "Robot Ted and Todd will sometimes drop dirty chicken wings", "Killing a Lawyer may result in a Bum or Undead Lawyers", "Shrink a BigBaby down and put him in a jar to create a Schlump", "The older your Schlump gets, the more valuable gifts he gives!", "Do not throw eggs at Ponies! You have been warned!", "Some Prisoners are friendly and will reward you upon release!", "Some Prisoners are just evil and will attack you on sight!", "Evil Scientists will conduct experiments that sometimes backfire.", "Your pet loses a level if resurrected with a LifeGem.", "Sneaky Sal will sometimes sell goods at a discount.", "Now under new Management!" };
	
	@SidedProxy(clientSide="nz.co.crookedhill.mcwr.proxy.MCWRClientProxy",serverSide="nz.co.crookedhill.mcwr.proxy.MCWRCommonProxy")
	public static MCWRCommonProxy proxy;
	
	public static CreativeTabs mcwrItems = new MCWRCreativeTabItem(CreativeTabs.getNextID(), MODID);
	public static CreativeTabs mcwrMobs = new MCWRCreativeMobs(CreativeTabs.getNextID(), MODID);
	public static Map<String, Item> items;
	@EventHandler
	public void preinit(FMLPreInitializationEvent event)
	{
		MCWRClassGetter getter = new MCWRClassGetter();
		
		items = getter.getItems(ITEMDIR);
		registerItems(items);
		MCWRAchievement.init();	
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		
	}
	
	@EventHandler
	public void postinit(FMLPostInitializationEvent event)
	{
		//MinecraftForge.EVENT_BUS.register(MCWRConnectionHandler.class);
		FMLCommonHandler.instance().bus().register(MCWRConnectionHandler.class);
	}
	
	private void registerItems(Map<String, Item> items)
	{
		for(Map.Entry<String, Item> entry : items.entrySet())
			GameRegistry.registerItem(entry.getValue(), entry.getKey());
	}
}
