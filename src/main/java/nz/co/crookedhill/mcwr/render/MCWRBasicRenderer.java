package nz.co.crookedhill.mcwr.render;

import java.io.File;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.util.ResourceLocation;
import nz.co.crookedhill.mcwr.MCWR;

public class MCWRBasicRenderer extends RenderLiving{

	protected ResourceLocation texture;
	
	public MCWRBasicRenderer(ModelBase modelBase, float shadowSize, String textureName) {
		super(modelBase, shadowSize);
		texture = new ResourceLocation(MCWR.MODID + ":textures"+File.separator + "mobs" + File.separator + textureName + ".png");
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity) {
		return null;
	}
	
	protected void preRenderCallbackEntity(Entity entity, float f)
    {
		//Change Entity entity to the entity of choice
        // some people do some G11 transformations or blends here, like you can do
        // GL11.glScalef(2F, 2F, 2F); to scale up the entity
        // which is used for Slime entities.  I suggest having the entity cast to
        // your custom type to make it easier to access fields from your 
        // custom entity, eg. GL11.glScalef(entity.scaleFactor, entity.scaleFactor, 
        // entity.scaleFactor); 
    }
	/**
	 * if you want to do some fancy stuff with opengl, like make the mob bigger/smaller 
	 * etc, overide this method and preRenderCallbackSerpent
	 */
	@Override
    protected void preRenderCallback(EntityLivingBase entity, float f)
    {
        //preRenderCallbackSerpent((EntitySerpent) entity, f);
    }
	
}
