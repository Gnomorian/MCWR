package nz.co.crookedhill.mcwr.creativetabs;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import nz.co.crookedhill.mcwr.MCWR;

public class MCWRCreativeTabItem extends CreativeTabs
{
	public MCWRCreativeTabItem(int tabID, String label)
    {
		super(tabID, label);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public String getTranslatedTabLabel()
    {
	return "MoreCreeps Items";
    }

    @Override
    public Item getTabIconItem()
    {
    	return MCWR.items.get("blorpcola");
    }
}
