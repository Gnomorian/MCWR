package nz.co.crookedhill.mcwr.creativetabs;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import nz.co.crookedhill.mcwr.MCWR;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class MCWRCreativeMobs extends CreativeTabs
{
	public MCWRCreativeMobs(int tabID, String label)
    {
		super(tabID, label);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public String getTranslatedTabLabel()
    {
	return "MoreCreeps Creeps";
    }

    @Override
    public Item getTabIconItem()
    {
    	return MCWR.items.get("spawnfloob");
    }
}
