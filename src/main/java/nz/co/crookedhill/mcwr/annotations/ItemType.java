package nz.co.crookedhill.mcwr.annotations;

public @interface ItemType {

	String value();

}
