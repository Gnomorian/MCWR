package nz.co.crookedhill.mcwr.helper;

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. 
 */

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import com.google.common.reflect.ClassPath.ClassInfo;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import nz.co.crookedhill.mcwr.MCWR;

/**
 * this class is a utility for loading objects into 
 * your minecraft mod without the mod having to know 
 * where it is.
 * 
 * it is recommended that you only use it if you have 
 * allot of items or entities as this may cause a small 
 * performance hit, but only as much as mods hit 
 * themselves already.
 * 
 * @author William Cameron (Gnomorian)
 * @email  w.cameron@crookedhill.co.nz
 * @Website http://www.crookedhill.co.nz/
 */
public class MCWRClassGetter {
	/**
	 * @param classpath the path to your item classes e.g. "nz.co.crookedhill.items"
	 * @return a hashmap of your items, unlocalized name is the key.
	 * @throws  
	 */
	public HashMap<String, Item> getItems(String pack)
	{
		HashMap<String, Item> items = new HashMap<String, Item>();


			ImmutableSet<ClassInfo> set;
			try {
				set = ClassPath.from(this.getClass().getClassLoader()).getTopLevelClassesRecursive(pack);
				System.out.println("the sit is " + set.size() + "long.");
				for(ClassInfo cinfo : set)
				{
					Class<?> clas = Class.forName(cinfo.getName());
					Constructor<?> constr = clas.getConstructor(CreativeTabs.class);
					Item item = (Item)constr.newInstance(MCWR.mcwrItems);
					items.put(item.getUnlocalizedName().replace("item.", ""), item);
				}
				System.out.println(items.size() + " items found in " + pack);
			
			} catch (IOException e) {
				System.out.println("Error with loading list of objects in the package \"" + pack + "\" you provided.");
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				System.out.println("Error with loading Class within the package you proveded, it may not be a valid class.");
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				System.out.println("Error with passing arguments to a class, the constructor shoudl be: public ClassName(CreativeTabs creativetab){}");
				e.printStackTrace();
			} catch (SecurityException e) {
				System.out.println("Error with passing arguments to a class, is the constructor public?");
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				System.out.println("Error with instantiating the class, is the class public?");
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
			
		return items;
	}
	
}
