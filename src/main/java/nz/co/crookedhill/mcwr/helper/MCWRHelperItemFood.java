package nz.co.crookedhill.mcwr.helper;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import nz.co.crookedhill.mcwr.MCWR;

public abstract class MCWRHelperItemFood extends ItemFood
{
	/**
	 * helper class for ide to auto import useful functions.
	 * @param unlololizedName system name of the item.
	 * @param creativeTab tab for the item to reside in.
	 * @param nourishmentAmount amount of hunger points this item replenishes.
	 * @param alwaysEdible can you always eat it? (dont quite get this one, but then i havent tested it.)
	 */
	public MCWRHelperItemFood(String unlololizedName,CreativeTabs creativeTab, int nourishmentAmount, boolean alwaysEdible) 
	{
		super(nourishmentAmount, alwaysEdible);
		this.setUnlocalizedName(unlololizedName);
		this.setCreativeTab(creativeTab);
		this.setTextureName(MCWR.MODID+":" + "item_" + unlololizedName);
	}
	/**
	 * when you destroy a block and this is in the hand, trigger this.
	 * default return value if you dont want to use it is false
	 */
	public abstract boolean onBlockDestroyed(ItemStack itemstack, World world, Block block, int x, int y, int z, EntityLivingBase entity);
	
	/**
	 * define all recipes for the item here.
	 * due to the nature of the loading of classes,
	 * the item hosts everything to do with itself.
	 * 
	 * i recommend using the .contains method to see
	 * if the items hashmap contains items for this mod,
	 * if it does, make the recipe.
	 */
	public abstract void createRecipes();
	
	public abstract boolean onBlockStartBreak(ItemStack itemstack, int X, int Y, int Z, EntityPlayer player);
	
	public abstract void onCreated(ItemStack itemstack, World world, EntityPlayer player);
	
	public abstract boolean onDroppedByPlayer(ItemStack item, EntityPlayer player);
	
	public abstract boolean onItemUse(ItemStack itemstack, EntityPlayer player, World world, int x, int y, int z, int p_77648_7_, float p_77648_8_, float p_77648_9_, float p_77648_10_);
	
	public abstract boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ);

	public abstract boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity);
	
	public abstract void onPlayerStoppedUsing(ItemStack itemstack, World world, EntityPlayer player, int p_77615_4_);}
