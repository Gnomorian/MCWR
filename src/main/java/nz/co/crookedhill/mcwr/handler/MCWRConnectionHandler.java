package nz.co.crookedhill.mcwr.handler;

import java.util.Random;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import nz.co.crookedhill.mcwr.MCWR;

public class MCWRConnectionHandler
{
	@SubscribeEvent
	public void onJoinEvent(EntityJoinWorldEvent event)
	{
		System.out.println("this runs");
		if(event.entity instanceof EntityPlayer)
		{
			System.out.println("entity is a player");
			Random rand = new Random();
			((EntityPlayer)event.entity).addChatMessage(new ChatComponentText(MCWR.WELCOME[rand.nextInt(MCWR.WELCOME.length)]));
		}
	}
}
