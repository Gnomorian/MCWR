package nz.co.crookedhill.mcwr.items;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import nz.co.crookedhill.mcwr.MCWR;
import nz.co.crookedhill.mcwr.helper.MCWRHelperItemFood;

public class MCWRItemBlorpCola extends MCWRHelperItemFood
{
	public MCWRItemBlorpCola(CreativeTabs tab)
	{	/* healAmount, alwaysEatable */
		super("blorpcola", tab, 2, true);
	}
	/*
	 * on consumption play blorpcola.ogg.
	 * on consumption give blorpcola achievement.
	 */

	@Override
	public boolean onBlockDestroyed(ItemStack itemstack, World world,
			Block block, int x, int y, int z, EntityLivingBase entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onBlockStartBreak(ItemStack itemstack, int X, int Y, int Z,
			EntityPlayer player) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onCreated(ItemStack p_77622_1_, World p_77622_2_,
			EntityPlayer p_77622_3_) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onDroppedByPlayer(ItemStack item, EntityPlayer player) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onItemUse(ItemStack p_77648_1_, EntityPlayer p_77648_2_,
			World p_77648_3_, int p_77648_4_, int p_77648_5_, int p_77648_6_,
			int p_77648_7_, float p_77648_8_, float p_77648_9_,
			float p_77648_10_) {
		// TODO Auto-generated method stub
		System.out.println(MCWRItemBlorpCola.class.getName());
		return false;
	}

	@Override
	public boolean onItemUseFirst(ItemStack stack, EntityPlayer player,
			World world, int x, int y, int z, int side, float hitX, float hitY,
			float hitZ) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player,
			Entity entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onPlayerStoppedUsing(ItemStack p_77615_1_, World p_77615_2_,
			EntityPlayer p_77615_3_, int p_77615_4_) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createRecipes() {
		// TODO Auto-generated method stub
		
	}
}
