package nz.co.crookedhill.mcwr.achievement;

import net.minecraft.stats.Achievement;
import net.minecraftforge.common.AchievementPage;
import nz.co.crookedhill.mcwr.MCWR;

public class MCWRAchievement 
{
	
	public static void init()
	{
		AchievementPage.registerAchievementPage(new AchievementPage("MoreCreeps Reborn",new Achievement[]{
				new Achievement("achievefrisbee", "achievefrisbee", 0, 0, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieveradio", "achieveradio", 0, 1, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievegotohell", "achievegotohell", 0, 2, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievechugcola", "achievechugcola", 0, 3, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achievepigtaming", "achievepigtaming", 0, 4, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievepiglevel5", "achievepiglevel5", 0, 5, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievepiglevel10", "achievepiglevel10", 0, 6, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievepiglevel20", "achievepiglevel20", 0, 7, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievepighotel", "achievepighotel", 0, 8, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achieverocketgiraffe", "achieverocketgiraffe", 1, 0, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieverocket", "achieverocket", 1, 2, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieverocketrampage", "achieverocketrampage", 1, 3, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achievelolliman", "achievelolliman", 1, 4, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievepyramid", "achievepyramid", 1, 5, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievefloobkill", "achievefloobkill", 1, 6, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievefloobicide", "achievefloobicide", 1, 7, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achievegookill", "achievegookill", 2, 0, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievegookill10", "achievegookill10", 2, 1, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievegookill25", "achievegookill25", 2, 2, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achievesnowdevil", "achievesnowdevil", 2, 3, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievehunchback", "achievehunchback", 2, 4, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieverockmonster", "achieverockmonster", 2, 5, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achievebumflower", "achievebumflower", 2, 6, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievebumpot", "achievebumpot", 2, 7, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievebumlava", "achievebumlava", 2, 8, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achieve100bucks", "achieve100bucks", 3, 0, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieve500bucks", "achieve500bucks", 3, 1, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieve1000bucks", "achieve1000bucks", 3, 2, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achieve10bubble", "achieve10bubble", 3, 3, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieve25bubble", "achieve25bubble", 3, 4, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieve50bubble", "achieve50bubble", 3, 5, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieve100bubble", "achieve100bubble", 3, 6, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				
				
				new Achievement("achievesnow", "achievesnow", 4, 0, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievesnowtiny", "achievesnowtiny", 4, 1, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievesnowtall", "achievesnowtall", 4, 2, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achievehotdogtaming", "achievehotdogtaming", 4, 3, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievehotdoglevel5", "achievehotdoglevel5", 4, 4, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievehotdoglevel10", "achievehotdoglevel10", 4, 5, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievehotdoglevel25", "achievehotdoglevel25", 4, 6, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievehotdogheaven", "achievehotdogheaven", 4, 7, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achieveram128", "achieveram128", 5, 0, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieveram512", "achieveram512", 5, 1, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieveram1024", "achieveram1024", 5, 2, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achievefalseidol", "achievefalseidol", 5, 3, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievecamel", "achievecamel", 5, 4, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievezebra", "achievezebra", 5, 5, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieveschlump", "achieveschlump", 5, 6, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achievenonswimmer", "achievenonswimmer", 5, 7, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achieveprisoner", "achieveprisoner", 6, 0, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieve5prisoner", "achieve5prisoner", 6, 1, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieve10prisoner", "achieve10prisoner", 6, 2, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achieve1caveman", "achieve1caveman", 6, 3, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieve10caveman", "achieve10caveman", 6, 4, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieve50caveman", "achieve50caveman", 6, 5, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achieve1cavemanice", "achieve1cavemanice", 6, 6, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieve5cavemanice", "achieve5cavemanice", 6, 7, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieve10cavemanice", "achieve10cavemanice", 6, 8, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achieve1horse", "achieve1horse", 7, 0, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieve5horse", "achieve5horse", 7, 1, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieve10horse", "achieve10horse", 7, 2, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				
				new Achievement("achieveponylevel5", "achieveponylevel5", 7, 3, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieveponylevel10", "achieveponylevel10", 7, 4, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieveponylevel25", "achieveponylevel25", 7, 5, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat(),
				new Achievement("achieveponylevel50", "achieveponylevel50", 7, 6, MCWR.items.get("item.blorpcola"), null).initIndependentStat().registerStat()
		}));
	}
}
